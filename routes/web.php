<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index.html', function () {
    return view('index');
});

Route::get('/offer1.html', function () {
    return view('offer1');
});

Route::get('/offer2.html', function () {
    return view('offer2');
});

Route::get('/offer3.html', function () {
    return view('offer3');
});

Route::get('/offer4.html', function () {
    return view('offer4');
});

//Route::get('tasks', 'TasksController@index')->name('tasks.index');
//
//Route::get('tasks/create', 'TasksController@create')->name('tasks.create');
//
//Route::post('tasks/store', 'TasksController@store')->name('tasks.store');
//
//Route::get('tasks/{id}/edit', 'TasksController@edit')->name('tasks.edit');
//
//Route::put('tasks/{id}/update', 'TasksController@update')->name('tasks.update');
//
//Route::get('tasks/{id}/show', 'TasksController@show')->name('tasks.show');
//
//Route::delete('tasks/{id}/destroy', 'TasksController@destroy')->name('tasks.destroy');

Route::resource('tasks', 'TasksController');

//Route::get('users', function () {
//    $users = App\User::paginate(10);
//    $users->withPath('$id');
//
//    //
//});