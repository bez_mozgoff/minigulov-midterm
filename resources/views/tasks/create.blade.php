@extends('layout')

@section('content')

    <div class="container">
        <div class="row">
            <h3>Add new rating</h3>
            <hr>
            {{--{{dd($errors)}}--}}
            @include('error')

            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['route' => ['tasks.store']]) !!}

                    <div class="form-group">
                        <h5>Choose your rating:</h5>
                        <select class="form-control" id="" name='title'>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                        <h5>Write some description:</h5>
                        <textarea name="description" id="" cols="30" rows="10" class="form-control">{{ old('description') }}</textarea>
                        <br>
                        <button class="btn btn-success">Submit</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection