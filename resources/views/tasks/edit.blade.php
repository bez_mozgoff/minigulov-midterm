@extends('layout')

@include('error')

@section('content')
<div class="container">
    <h3>Edit rating # - {{ $task->id }}</h3>
    <div class="row">
        <div class="col-md-12">

            {!! Form::open(['route' => ['tasks.update', $task->id], 'method'=>'PUT']) !!}

            <div class="form-group">
                <select class="form-control" id="" name='title'>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
                    <br>
                <textarea name="description" id="" cols="30" rows="10" class="form-control">{{$task->description}}</textarea>
                <br>
                <button class="btn btn-warning">Submit</button>
            </div>

            {!! Form::close() !!}

        </div>

    </div>
</div>
@endsection