@extends('layout')


@section('content')

    <div class="container">
        <div class="row">
            <h3>Rating list</h3>
            <a href="{{ route('tasks.create') }}" class="btn btn-success">Rate</a>
            <div class="col-md-10 col-md-offset-1">
                <table class="table">
                    <thread>
                        <tr>
                            <td>ID</td>
                            <td>Rate</td>
                            <td>Actions</td>
                        </tr>
                    </thread>
                    <tbody>
                    @foreach($tasks as $task)
                    <tr>
                        <td>{{$task->id}}</td>
                        <td>{{$task->title}}</td>
                        <td>
                            <a href="{{ route('tasks.show', $task->id) }}">
                                <i class="glyphicon glyphicon-eye-open"></i>
                            </a>
                            <a href="{{ route('tasks.edit', $task->id) }}">
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>
                            <a href="#">

                            </a>
                            {!! Form::open(['method' => 'DELETE',
                            'route' => ['tasks.destroy', $task->id]]) !!}
                            <button onclick="return confirm('Are you sure want to remove it?')"> <i class="glyphicon glyphicon-remove"></i> </button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    {{ $tasks->links() }}
                </table>
            </div>
        </div>
    </div>
@endsection